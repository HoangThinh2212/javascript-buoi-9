function layThongTinTuForm() {
  const ma = document.getElementById("txtMaSV").value;
  const ten = document.getElementById("txtTenSV").value;
  const loai = document.getElementById("loaiSV").value;
  const diemToan = document.getElementById("txtDiemToan").value * 1;
  const diemVan = document.getElementById("txtDiemVan").value * 1;

  var sv = {
    maSV: ma,
    loaiSV: loai,
    tenSV: ten,
    diemToanSV: diemToan,
    diemVanSV: diemVan,
    //update

    tinhDTB: function () {
      return (this.diemToanSV + this.diemVanSV) / 2;
    },

    xepLoai: function () {
      let dtb = this.tinhDTB();
      if (dtb > 5) {
        return "Good";
      } else {
        return "Bad";
      }
    },
  };
  return sv;
}

function showThongTinLenForm(sv) {
  document.getElementById("spanTenSV").innerText = sv.tenSV;
  document.getElementById("spanMaSV").innerText = sv.maSV;
  document.getElementById("spanLoaiSV").innerText = sv.loaiSV;

  document.getElementById("spanDTB").innerText = sv.tinhDTB();

  document.getElementById("spanXepLoai").innerText = sv.xepLoai();
}
