function Cat(catName, catAge) {
    this.username = catName;
    this.number = catAge;
    this.talk = function () {
      console.log("I am a good cat", this.username);
    };
  }
  var cat1 = {
    name: "Alice",
    age: 2,
  };
  
  var cat2 = new Cat("Tom", 5);
  console.log("cat2: ", cat2.username);
  cat2.talk();
  