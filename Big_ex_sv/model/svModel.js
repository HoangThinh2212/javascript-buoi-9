function SinhVien(maSv, tenSv, email, matKhau, toan, ly, hoa) {
  this.ma = maSv;
  this.ten = tenSv;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDTB = function () {
    // Tinh DTB
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}
