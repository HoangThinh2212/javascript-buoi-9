// kiểm tra Trùng
function kiemTraTrung(id, dssv) {
  let index = timKiemViTri(id, dssv);
  if (index !== -1) {
    showMessageErr("spanMaSV", "Mã Sinh Viên đã tồn tại");
    return false;
  } else {
    showMessageErr("spanMaSV", "");
    return true;
  }
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d+$/;

  let isNumber = reg.test(value);
  if (isNumber) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Email không đúng định dạng");
    return false;
  }
}
