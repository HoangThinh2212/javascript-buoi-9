const DSSV = "DSSV";
var dssv = [];

function luuLocalStorage() {
  let jsonDssv = JSON.stringify(dssv);
  localStorage.setItem("DSSV", jsonDssv);
}
// localStorage.getItem: Lấy dữ liệu từ LocalStorage khi load trang ~ localStorage chỉ lưu trữ JSON
var dataJson = localStorage.getItem("DSSV");
console.log("dataJson: ", dataJson);
if (dataJson !== null) {
  // nếu có dữ liệu thì convert từ json thành array lại
  var svArr = JSON.parse(dataJson);
  // convert data vì dữ liệu không có method
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }
  renderDssv(dssv);
}
function themSv() {
  var sv = layThongTinTuForm();

  var isValid = true;
  isValid =
    // Đàu tiên Validate maSV
    kiemTraRong(sv.ma, "spanMaSV", "Mã sinh viên không được để rỗng") &&
    kiemTraTrung(sv.ma, dssv) &&
    kiemTraSo(sv.ma, "spanMaSV", "Mã sinh viên phải là số");

  // sau đó Validate tiếp tên sinh viên
  isValid =
    isValid &
    kiemTraRong(sv.ten, "spanTenSV", "Tên sinh viên không được để rỗng");

  // Sau đó Validate Email
  isValid = isValid & kiemTraEmail(sv.email, "spanEmailSV");

  if (isValid) {
    // Add vào Array
    dssv.push(sv);

    // Lưu vào Storage
    luuLocalStorage();

    //   Render danh sách sinh viên
    renderDssv(dssv);
  }
}

// thêm function xóa Sv
function xoaSv(id) {
  console.log("id: ", id);
  // splice
  var viTri = timKiemViTri(id, dssv);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    dssv.splice(viTri, 1);
    // lưu lại vào Local Storage
    luuLocalStorage();
    // render lại giao diện sau khi xóa
    renderDssv(dssv);
  }
}

function suaSv(id) {
  var viTri = timKiemViTri(id, dssv);
  console.log(" viTri: ", viTri);
  if (viTri == -1) return;

  var data = dssv[viTri];
  console.log(" data: ", data);
  showThongTinLenForm(data);

  //Ngăn cản user chỉnh sửa id
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  var data = layThongTinTuForm();
  console.log("data: ", data);

  var viTri = timKiemViTri(data.ma, dssv);
  if (viTri == -1) return;

  dssv[viTri] = data;
  renderDssv(dssv);
  luuLocalStorage();

  //remove disable
  document.getElementById("txtMaSV").disabled = false;
  resetForm();
}
