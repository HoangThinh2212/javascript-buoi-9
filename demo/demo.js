// Pass by reference : array, object
var name = "Alice";
var age = 2;
var gender = "Female";
var sv1 = {
  name: "Alice",
  age: 2,
  gender: "Female",
};
var sv2 = {
  name: "Bob",
  age: 2,
  gender: "Male",
};
console.log("sv1 before: ", sv1);

sv1.age = 5;
console.log("sv1 after: ", sv1);

var cat1 = {
  name: "Black",
  score: 10,
  talk: function(){
    console.log("Gâu gâu, I am", this.name);
  }
};
cat1.talk();
//dynamic key
var key = "name"; // name ~ key mong muốn linh động update
cat1[key] = "Black Adam";
console.log("cat1: ", cat1);
// Bình Thường làm
// cat1.name = "Black Adam";

const cat2 = cat1;
console.log("Before");
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);
cat2.score = 1;
cat2.isLogin = true;
console.log("After ");
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);

var a = 5;
var b = a;
b = 10;
console.log("a: ", a);

// object = {key:value}
